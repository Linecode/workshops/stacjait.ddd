using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using MyVod.Domain.Movies.Events;

namespace MyVod.Domain.Converter.Handlers.Events
{
    public class MovieCreatedListener : INotificationHandler<MovieCreated>
    {
        private readonly ILogger<MovieCreatedListener> _logger;

        public MovieCreatedListener(ILogger<MovieCreatedListener> logger)
        {
            _logger = logger;
        }
        
        public async Task Handle(MovieCreated notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation($"Started processing Movie: {notification.Movie.Id} {notification.Movie.Title}");
            await Task.Delay(2000, cancellationToken);
            _logger.LogInformation($"Completed processing Movie: {notification.Movie.Id} {notification.Movie.Title}");
        }
    }
}