using Linecode.DDD;

namespace MyVod.Domain.Movies.Policies
{
    public static class PublishPolicies
    {
        public static PublishPolicy Default => new PriceCheck();
        
        private class PriceCheck : PublishPolicy
        {
            public Result Publish(Movie movie)
                => movie.Price != null ? Result.Success() : Result.Error(new PriceCheckPolicyFailure());
        }
    }
}