using Linecode.DDD;
using MediatR;

namespace MyVod.Domain.Movies.Commands
{
    public record PublishMovieCommand(MovieId MovieId) : IRequest<Result>;
}