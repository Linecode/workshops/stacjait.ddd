using Linecode.DDD;

namespace MyVod.Domain.Movies
{
    public sealed class CastMember : Entity<CastMemberId>
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        
        public CastMember(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
            
            Id = CastMemberId.New();
        }
    }
}