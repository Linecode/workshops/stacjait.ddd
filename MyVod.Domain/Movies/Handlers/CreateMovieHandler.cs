using System;
using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using MediatR;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Repositories;

namespace MyVod.Domain.Movies.Handlers
{
    public class CreateMovieHandler : IRequestHandler<CreateMovieCommand, Result<MovieId>>
    {
        private readonly IMoviesRepository _moviesRepository;

        public CreateMovieHandler(IMoviesRepository moviesRepository)
        {
            _moviesRepository = moviesRepository;
        }
        
        public async Task<Result<MovieId>> Handle(CreateMovieCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var movie = new Movie(request.Title, request.Description);
            
                _moviesRepository.Add(movie);
                await _moviesRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                return Result<MovieId>.Success(movie.Id);
            }
            catch (Exception e)
            {
                return Result<MovieId>.Error(e);
            }
        }
    }
}