using System;
using System.Threading;
using System.Threading.Tasks;
using Linecode.DDD;
using Linecode.DDD.Exceptions;
using MediatR;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Repositories;

namespace MyVod.Domain.Movies.Handlers
{
    public class SetMoviePriceHandler : IRequestHandler<SetMoviePriceCommand, Result>
    {
        private readonly IMoviesRepository _repository;

        public SetMoviePriceHandler(IMoviesRepository repository)
        {
            _repository = repository;
        }
        
        public async Task<Result> Handle(SetMoviePriceCommand request, CancellationToken cancellationToken)
        {
            var movie = await _repository.Get(request.MovieId);
            
            if (movie is null)
                return Result<Result>.Error(new NotFoundException());

            var @event = new Movie.Events.SetPriceEvent(request.Money);

            try
            {
                movie.Apply(@event);
                await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
                return Result.Success();
            }
            catch (Exception e)
            { 
                return Result.Error(e);
            }
        }
    }
}