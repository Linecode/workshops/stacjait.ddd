using System;
using System.Collections.Generic;
using Linecode.DDD;
using MyVod.Domain.Movies.Events;
using MyVod.Domain.Movies.Policies;
using MyVod.Domain.Shared;

namespace MyVod.Domain.Movies
{
    public sealed class Movie : AggregateRoot<MovieId>
    {
        public string Title { get; private set; }
        public string Description { get; private set; }
        public Money Price { get; private set; }
        public MovieStatus Status { get; private set; }

        [Obsolete("Only for EFCore", true)]
        private Movie(){}
        
        public Movie(string title, string description)
        {
            Title = title;
            Description = description;

            Id = MovieId.New();
            Status = MovieStatus.Created;
            
            Raise(new MovieCreated(this));
        }

        internal void Apply(Events.PublishEvent @event)
        {
            var result = @event.Policy.Publish(this);

            if (result.IsSuccessful)
            {
                Status = MovieStatus.Published;
                
                Raise(new MoviePublished(this));
            }
            else
            {
                throw result.Exception;
            }
        }

        internal void Apply(Events.SetPriceEvent @event)
        {
            Price = @event.NewPrice;
        }

        internal static class Events
        {
            internal record SetPriceEvent(Money NewPrice);
            // internal class SetPriceEvent
            // {
            //     public Money NewPrice { get; set; }
            // }

            internal class PublishEvent
            {
                public PublishPolicy Policy { get; set; }
            }
        }

        public enum MovieStatus
        {
            Created,
            Published
        }
    }
}