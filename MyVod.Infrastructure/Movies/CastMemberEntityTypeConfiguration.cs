using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyVod.Domain.Movies;

namespace MyVod.Infrastructure.Movies
{
    public class CastMemberEntityTypeConfiguration : IEntityTypeConfiguration<CastMember>
    {
        public void Configure(EntityTypeBuilder<CastMember> builder)
        {
            builder.HasKey(x => x.Id);
            
            builder.Property(x => x.Id)
                .HasConversion(id => id.Value,
                    s => new CastMemberId(s));
            
            builder.Ignore(x => x.DomainEvents);

            builder.Property(x => x.FirstName);
            builder.Property(x => x.LastName);
            
            builder.ToTable("CastMembers", MoviesContext.Schema);
        }
    }
}