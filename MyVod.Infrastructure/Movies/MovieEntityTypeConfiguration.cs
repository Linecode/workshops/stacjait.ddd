using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MyVod.Domain.Movies;
using MyVod.Domain.Shared;

namespace MyVod.Infrastructure.Movies
{
    public class MovieEntityTypeConfiguration : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(x => x.Id);
            
            builder.Property(x => x.Id)
                .HasConversion(id => id.Value,
                    s => new MovieId(s));

            builder.Ignore(x => x.DomainEvents);

            builder.Property(x => x.Title);
            builder.Property(x => x.Description);
            builder.Property(x => x.Status)
                .HasConversion<string>();

            builder.OwnsOne(x => x.Price, p =>
            {
                p.Property(x => x.Currency)
                    .HasConversion<string>()
                    .HasColumnName("Price_Currency");

                p.Property(x => x.Value)
                    .HasColumnName("Price_Value");
            });

            builder.ToTable("Movies", MoviesContext.Schema);
        }
    }
}