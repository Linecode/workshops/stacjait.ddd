using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Policies;
using MyVod.Domain.Shared;
using Xunit;

namespace MyVod.Domain.Tests.Movies
{
    public class Movie_Tests
    {
        [Fact]
        public void Movie_Should_Contains_New_Price_After_Set_Price_Command_Is_Applied()
        {
            var movie = GetAMovie();
            var money = new Money(12m, Money.Currencies.EUR);
            var @event = new Movie.Events.SetPriceEvent(money);
            
            movie.Apply(@event);
        
            var movieShould = new MovieAssert(movie);

            movieShould
                .HaveTitleAndDescription("title", "description")
                .HavePrice(money);
        }
        
        [Fact]
        public void Movie_Should_Be_Published_After_Publish_Event_Is_Applied()
        {
            var movie = GetAMovie();
            var money = new Money(12m, Money.Currencies.EUR);
            var priceEvent = new Movie.Events.SetPriceEvent(money);
            movie.Apply(priceEvent);
            var publishEvent = new Movie.Events.PublishEvent
            {
                Policy = PublishPolicies.Default
            };
            
            movie.Apply(publishEvent);
        
            var movieShould = new MovieAssert(movie);
        
            movieShould
                .BePublished();
        }
        
        [Fact]
        public void Movie_Should_Throw_Exception_If_Publish_Is_Called_Without_Price()
        {
            var movie = GetAMovie();
            var publishEvent = new Movie.Events.PublishEvent
            {
                Policy = PublishPolicies.Default
            };
        
            Assert.Throws<PriceCheckPolicyFailure>(() =>
            {
                movie.Apply(publishEvent);
            });
        }

        private Movie GetAMovie()
            => new Movie("title", "description");
    }
}